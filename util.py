import cv2
import numpy as np
from tqdm import tqdm
from skimage.metrics import mean_squared_error


def get_hed(img_rgb, blur_window=5, scalefactor=1.0):
    model_path = './model/hed_pretrained_bsds.caffemodel'
    prototxt_path = './model/deploy.prototxt'

    net = cv2.dnn.readNetFromCaffe(prototxt_path, model_path)

    H, W = img_rgb.shape[:2]

    img = cv2.GaussianBlur(img_rgb, (blur_window, blur_window), 0)

    blob = cv2.dnn.blobFromImage(img, scalefactor, size=(W, H), mean=(105, 117, 123), swapRB=False, crop=False)

    net.setInput(blob)
    hed_output = net.forward()
    hed = hed_output[0, 0]

    return hed


def auto_canny(img_gray, hed):
    img = cv2.GaussianBlur(img_gray, (5, 5), 0)
    median = np.median(img)

    def get_bound(sigma):
        lower = int(max(0, (1.0 - sigma) * median))
        upper = int(min(255, (1.0 + sigma) * median))
        return lower, upper

    sigma_values = np.linspace(0.001, 1.0, 100)
    best_sigma = None
    min_mse = float('inf')
    mse_values = []

    for sigma in tqdm(sigma_values, desc="Searching for best sigma", unit="sigma"):
        lower, upper = get_bound(sigma)
        img_autocanny = cv2.Canny(img, lower, upper)
        current_mse = mean_squared_error(hed, img_autocanny)
        mse_values.append(current_mse)
        if current_mse < min_mse:
            min_mse = current_mse
            best_sigma = sigma

    lower, upper = get_bound(best_sigma)
    edge_img = cv2.Canny(img, lower, upper)

    return edge_img, (best_sigma, min_mse, lower, upper), (sigma_values, mse_values)


class CropLayer(object):
    def __init__(self, params, blobs):
        self.xstart = 0
        self.xend = 0
        self.ystart = 0
        self.yend = 0

    def getMemoryShapes(self, inputs):
        inputShape, targetShape = inputs[0], inputs[1]
        batchSize, numChannels = inputShape[0], inputShape[1]
        height_in, width_in = inputShape[2], inputShape[3]
        height_out, width_out = targetShape[2], targetShape[3]

        self.ystart = (height_in - height_out) // 2
        self.xstart = (width_in - width_out) // 2

        self.yend = self.ystart + height_out
        self.xend = self.xstart + width_out

        return [[batchSize, numChannels, height_out, width_out]]

    def forward(self, inputs):
        return [inputs[0][:, :, self.ystart:self.yend, self.xstart:self.xend]]


cv2.dnn_registerLayer('Crop', CropLayer)
